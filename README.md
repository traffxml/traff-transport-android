[![Release](https://jitpack.io/v/org.traffxml/traff-transport-android.svg)](https://jitpack.io/#org.traffxml/traff-transport-android)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-transport-android/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-transport-android/javadoc/dev/.
